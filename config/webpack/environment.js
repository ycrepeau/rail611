const { environment } = require('@rails/webpacker')
const webpack = require("webpack")
environment.plugins.append("Provide", new webpack.ProvidePlugin({
  $: 'jquery',
  jQuery: 'jquery'
}))

const haml = require('./loaders/haml')
const coffee = require('./loaders/coffee')
const sass = require('./loaders/sass')
const scss = require('./loaders/scss')

environment.loaders.prepend('haml', haml)
environment.loaders.prepend('coffee', coffee)
environment.loaders.prepend('sass', sass)
environment.loaders.prepend('scss', scss)


module.exports = environment

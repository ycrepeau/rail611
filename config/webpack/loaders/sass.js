module.exports = {
  test: /\.sass(\.erb)?$/,
  use: [
    'css-loader',
    {
      loader: 'sass-loader',
      options: {
        sassOptions: {
          indentedSyntax: true
        }
      }
    }
  ]
}
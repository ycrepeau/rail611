module.exports = {
  test: /\.scss(\.erb)?$/,
  use: [
    'css-loader',
    {
      loader: 'sass-loader',
      options: {
        sassOptions: {
          indentedSyntax: false
        }
      }
    }
  ]
}